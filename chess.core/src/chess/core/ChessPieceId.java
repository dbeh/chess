package chess.core;
//TODO Vielleicht auf Deutsch übersetzen?
public enum ChessPieceId
{
    /**
     * König
     */
    KING,
    /**
     * Königin
     */
    QUEEN,
    /**
     * Turm
     */
    ROOK,
    /**
     * Läufer
     */
    BISHOP,
    /**
     * Pferd
     */
    KNIGHT,
    /**
     * Frau Bauer
     */
    PAWN
}
